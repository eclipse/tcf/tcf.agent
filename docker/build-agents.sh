#!/bin/sh

set -e
JOBS="-j $(nproc)"

make $JOBS MACHINE=x86_64 CC=clang
make $JOBS MACHINE=x86_64 OPSYS=MinGW CC=x86_64-w64-mingw32-gcc  MKDIR="mkdir -p \$(1)"
make $JOBS MACHINE=a64 CC=aarch64-linux-gnu-gcc
make $JOBS MACHINE=arm CC=arm-linux-gnueabihf-gcc
make $JOBS MACHINE=powerpc CC=powerpc64le-linux-gnu-gcc
make $JOBS MACHINE=powerpc CC=powerpc64-linux-gnu-gcc NO_SSL=1 NO_UUID=1 BINDIR=obj/GNU/Linux/powerpc-be/Debug
make $JOBS MACHINE=powerpc CC=powerpc-linux-gnu-gcc NO_SSL=1 NO_UUID=1 BINDIR=obj/GNU/Linux/powerpc-32-be/Debug
make $JOBS MACHINE=riscv64 CC=riscv64-linux-gnu-gcc NO_SSL=1 NO_UUID=1
